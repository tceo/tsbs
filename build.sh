#! /bin/bash

########################################
#      tceo's Simple Build Script      #
#––––––––––––––––––––––––––––––––––––––#
#     a very simple build "system"     #
########################################
# Usage:                               #
#  Running the script without any      #
#  arguments will display a help       #
#  message which informs you of        #
#  available build types, then         #
#  you can run the script with         #
#  one of the build types as           #
#  an argument.                        #
#                                      #
#  If you want to run the project      #
#  after compiling then you can set    #
#  the RUN environment variable to     #
#  a non-zero value.                   #
#                                      #
#  When using the build script for     #
#  a project editing the "build.vars"  #
#  file should be adequate for most    #
#  use cases. If you need something    #
#  more specific, you should use       #
#  some other build system.            #
#                                      #
# I have an issue/need a feature:      #
#  Feel free to open an issue at the   #
#  gitlab repo for this project:       #
#     https://gitlab.com/tceo/TSBS     #
#                                      #
#                                      #
#         Thank you for using          #
#     tceo's Simple Build Script       #
########################################

: "${VARIABLE_FILE:=build.vars}"
: "${FAIL_ON_ERROR:=true}"
: "${RUN:=0}"
: "${SUB:=""}"
: "${EDEFS:=""}"

# !! DO NOT TOUCH ANYTHING AFTER THIS LINE !!
CVRS=""
CFLGS=""
LL=""
LFLGS=""
B_TYPE="${1:-help}"
R_P=""
SBPS=()

error () {
    >&2 echo -e "\e[31m\e[1mERROR: $1\e[0m"
    $FAIL_ON_ERROR && exit 1
}

VARIABLE () {
    [ $# = 3 ] && { [ $1 = $B_TYPE ] || return; } && CVRS+="-D$2=$3 " && return
    [ $# = 2 ] && CVRS+="-D$1=$2 " && return
    error "Invalid argument count in VARIABLE definition."
}

COMPILER_FLAGS () {
    [ $# = 2 ] && { [ $1 = $B_TYPE ] || return; } && CFLGS+="$2 " && return
    [ $# = 1 ] && CFLGS+="$1 " && return
    error "Invalid argument count in COMPILER_FLAGS definition."
}

LINKER_FLAGS () {
    [ $# = 2 ] && { [ $1 = $B_TYPE ] || return; } && LFLGS+="$2 " && return
    [ $# = 1 ] && LFLGS+="$1 " && return
    error "Invalid argument count in LINKER_FLAGS definition."
}

LIBRARIES () {
    if [ $# = 2 ]; then
	[ $1 = $B_TYPE ] || return
	for L in ${2}; do
	    LL+="$(pkg-config --silence-errors --libs $L || echo -l$L) "
	done
	return
    elif [ $# = 1 ]; then
	for L in ${1}; do
	    LL+="$(pkg-config --silence-errors --libs $L || echo -l$L) "
	done
	return
    fi
    error "Invalid argument count in LIBRARIES definition."
}

RUN_WITH () {
    [ $# = 2 ] && { [ $1 = $B_TYPE ] || return; } && R_P=$2 && return
    [ $# = 1 ] && R_P=$1 && return
    error "Invalid argument count in RUN_WITH definition."
}

SUBPROJECT () {
    bt=$B_TYPE
    vr=""
    if [ $# = 2 ]; then
        vr=$2
    elif [ $# = 3 ]; then
        vr=$2
        bt=$3
    fi
    SBPS+=($1 $bt $vr)
}

if [ ! -e $VARIABLE_FILE ] && [[ $B_TYPE == *:* ]]; then
	VARIABLE_FILE=$(echo $B_TYPE | cut -d: -f1)
	B_TYPE=$(echo $B_TYPE | cut -d: -f2)
	source $VARIABLE_FILE.vars
else
	source $VARIABLE_FILE
fi

help () {
    echo "TCEO's Simple Build Script."
    echo " This is a simple script for setting up building for projects quickly."
    echo " This message is displayed when the argument given to the script does not correspond to any"
    echo " of the build types listed in the variables file."
    echo " If you have moved or renamed the variables file you need to edit the variable in the build script"
    echo " or pass a path to a variable file in the VARIABLE_FILE environment variable."
    echo "Currently registered build types: ${BUILD_TYPES[@]}"
    exit
}

[[ " ${BUILD_TYPES[@]} " =~ " ${B_TYPE} " ]] || help

for FLDR in ${HEADER_FOLDERS[@]}; do
    [ ! -d $FLDR ] && error "Header folder $FLDR not found!"
    CFLGS+="-I$FLDR "
done

FV=""
I=0
for FE in ${SOURCE_EXTENSIONS[@]}; do
    FV+="-name *.$FE"
    [ ${#SOURCE_EXTENSIONS[@]} = $((I+=1)) ] || FV+=" -or "
done

SFS=()
OFS=()
I=0
for F in ${SOURCE_FOLDERS[@]}; do
    [ ! -d $F ] && error "Source folder $F not found."
    SFS+=($(find $F $FV))
done

for F in ${INCLUDE_SOURCE_FILES[@]}; do
    [ -f $F ] || error "Non-existant file in INCLUDE_SOURCE_FILES $F."
    SFS+=($F)
done

[ ${#SFS[@]} = 0 ] && error "No source files found."

for SCR in ${PRE_COMPILE}; do
    [ -f $SCR ] || error "Pre-compile script $SCR not found."
    $SCR
done

for P in ${!SBPS[@]}; do
    me=$(readlink -f $0)
    mo=$(readlink -f $OUT_FOLDER)
    if (( $P % 2 )) || (( $P % 3)); then
        continue
    fi
    echo -e "\e[32mCompiling subproject \e[34m${SBPS[$P]}\e[32m.\e[0m"
    pushd ${SBPS[$P]} > /dev/null
    mkdir -p "$mo"
    SUB="$mo/" EDEFS=${SBPS[(($P + 2))]} $me ${SBPS[(($P + 1))]}
    if [ $? = 0 ]; then
        echo -e "\e[32mFinished compiling subproject \e[34m${SBPS[$P]}\e[32m.\e[0m"
    else
        error "Subproject ${SBPS[$P]} failed to compile."
    fi
    popd > /dev/null
done

if [ $SBPS ]; then
    LL="-L$OUT_FOLDER $LL"
fi

for F in ${INCLUDE_OBJECT_FILES[@]}; do
    [ -f $F ] || error "Non-existant file in INCLUDE_OBJECT_FILES $F."
    OFS+=($F)
done

for V in ${EDEFS[@]}; do
    CVRS+="-D$V"
done

mkdir -p $BUILD_FOLDER
I=0
for F in ${SFS[@]}; do
    RC=true
    BF=$(dirname $BUILD_FOLDER$F)
    [ ! -d $BF ] && mkdir -p $BF
    OFN=$BF/$(basename $F).o
    (($INCREMENTAL > 0)) && if [ -f $OFN ]; then
	if [ $INCREMENTAL = 2 ]; then
            [ $OFN -ot $F ] && RC=false
        else
            RC=false
            $COMPILER $CFLGS $CVRS -E -MD $F -o ${BUILD_FOLDER}temp.d && read UHL < ${BUILD_FOLDER}temp.d
	    UHL=($UHL)
            for HDR in ${UHL[@]:1}; do
               [ $OFN -ot $HDR ] && RC=true && break
            done
        fi
    fi
    echo -e "\e[32m[$((I+=1))/${#SFS[@]}]\e[34m $($RC && echo "Compiling" || echo "Unchanged"): \e[0m\e[1m$F\e[0m"
    $RC && $COMPILER $CFLGS $CVRS -c $F \
        -o $OFN
    OFS+=($OFN)
done

[ $INCREMENTAL = 1 ] && rm ${BUILD_FOLDER}temp.d

if [ $SUB ]; then
    OUT_FOLDER=$SUB
fi

echo -e "\e[34mLinking: \e[32m\e[1m'$OUTPUT_FILE'\e[0m"
$LINKER ${OFS[@]} \
	$LL \
	$LFLGS -o $OUT_FOLDER$OUTPUT_FILE

if [ ! $SUB ]; then # this is a bit dumb tbh.
    echo -e "\e[32m\e[1mFinished!\e[0m"
    if [ $RUN = 1 ]; then
        $R_P $OUTPUT_FILE
    fi
fi

I=0
if $CLEAN_AFTER_BUILD; then
    echo -e "\e[32mCleaning up.\e[0m"
    for F in ${OFS[@]}; do
	[ -f $F ] && rm $F && echo -e "\e[32m[$((I+=1))/${#SFS[@]}]\e[34m Removed: \e[0m\e[1m$F\e[0m"
    done
fi
