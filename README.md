# TCEO's Simple Build Script

I got tired of having to setup a build system every single time I started a new project, so I made my own.
The reasoning behind this is that I can now just throw these two files into a any folder and just start working
immediately.